'use strict';
(function (module) {
  try {
    module = angular.module('cwt.multi-select');
  } catch (e) {
    module = angular.module('cwt.multi-select', []);
  }
  var directiveName = 'cwtMultiSelect';
  var theDirective = function ($timeout) {
    return {
      restrict: 'E',
      templateUrl: 'templates/multiselect.html',
      require: ['ngModel', 'cwtMultiSelect'],
      controllerAs: 'ctrlMultiSelect',
      scope: {
        ngModel: '=',
        emptyText: '@',
        disabled: '=',
        isRequired: '=',
        loadingData: '=',
        readMode: '=',
        source: '=',
        name: '@',
        isflat: '@?',
        displayLocation: '@?',
        showSearchbar: '=?',
        displayProperty: '@?',
        hasError: '=?',
        errorMessage: '@',
        errorPlacement: '@',
        selectAllDisabled:'=?',
        searchProperty: '@?'
      },
      link: function (scope, element, attrs, controller) {
        scope.safeApply = function (fn) {
          var phase = this.$root.$$phase;
          if (phase === '$apply' || phase === '$digest') {
            if (fn && (typeof (fn) === 'function')) {
              fn();
            }
          } else {
            this.$apply(fn);
          }
        };
        controller[1].setNgModelController(controller[0]);
      },
      controller: ['$scope', '$element', '$timeout', function ($scope, $element, $timeout) {
        var ctrl = this,
          modelCtrl = null;

        if (!$scope.searchProperty) $scope.searchProperty = 'id';

        ctrl.setNgModelController = function (model) {
          modelCtrl = model;
        };

        if (!$scope.selectAllLabel) {
          $scope.selectAllLabel = 'Select all';
        }
        if (!$scope.selectAllMessage) {
          $scope.selectAllMessage = 'everything is selected';
        }

        function ngModelWatch(value) {
          if ($scope.sourceCopy) {
            $scope.traversData = '';
            if ($scope.isflat) {
              $scope.traversData = $scope.sourceCopy;
            } else {
              $scope.traversData = getAllchilds($scope.sourceCopy);
            }

            if (value && $scope.traversData) {
              var hulpArray = angular.copy(value);
              for (var i = 0; i < $scope.traversData.length; i++) {
                var index = value.indexOf($scope.traversData[i][$scope.searchProperty]);
                var indexHulp = hulpArray.indexOf($scope.traversData[i][$scope.searchProperty]);
                if (index > -1) {
                  hulpArray.splice(indexHulp, 1);
                  var id = value[index];
                  var item = $scope.traversData[i];
                  if (item) {
                    item.isChecked = true;
                  } else {
                    var index = $scope.ngModel.indexOf(id);
                    $scope.ngModel.splice(index, 1);
                  }
                } else {
                  $scope.traversData[i].isChecked = false;
                }
              }
              if (!$scope.isflat) {
                checkparent($scope.sourceCopy);
              }
              if (hulpArray.length > 0) {
                for (var i = 0; i < hulpArray.length; i++) {
                  var index = $scope.ngModel.indexOf(hulpArray[i][$scope.searchProperty]);
                  $scope.ngModel.splice(index, 1);
                }
              }
            }
          }
        }

        $scope.$watch('source', function () {
          $scope.sourceCopy = angular.copy($scope.source);
          ngModelWatch($scope.ngModel)
          if (!$scope.isflat) {
            _.forEach($scope.sourceCopy, function (child) {
              if (child.children && child.children.length !== 0) {
                child.isClosed = true;
              }
            });
          }
        })

        $scope.$watch('ngModel', ngModelWatch);

        ctrl.selectedItems = function () {
          if (!$scope.ngModel) {
            return [];
          }
          $scope.displayList = [];
          _.each($scope.ngModel, function (item) {
            if ($scope.traversData) {
              $scope.displayList.push(find(item, $scope.traversData));
            }
          })
          return $scope.ngModel;
        }
        function find(id, items, run) {
          var i = 0, found;
          if (items) {
            for (var i = 0; i < items.length; i++) {
              if (items[i][$scope.searchProperty] === id) {
                if (run) {
                  items[i].isChecked = false;
                }
                return items[i];
              } else if (_.isArray(items[i].children)) {
                found = find(id, items[i].children);
                if (found) {
                  return found;
                }
              }
            }
          } else {
            return null;
          }
        }

        function getAllchilds(item, childs, force) {
          var hulp = [];
          _.forEach(item, function (child) {
            if (child.children && !force && child.children.length > 0) {
              child.hasChildren = child.children.length > 0;
              var children = getAllchilds(child.children, childs);
              if (children.length > 0) {
                hulp = hulp.concat(children);
              }

            } else {
              hulp.push(child);
            }
          });
          return hulp;
        }

        function SetState(item, state) {
          if (item) {
            item.indeterminate = false;
            item.isChecked = false;
            if (state === 'indeterminate') {
              item.indeterminate = true;
            } else if (state === 'isChecked') {
              item.isChecked = true;
            }
          }
        }

        function checkparent(item, check) {
          var hulp = [];
          _.forEach(item, function (child) {
            if (child.children) {
              var children = checkparent(child.children, check);
              var selected = _.filter(children, function (item) {
                return item.isChecked;
              });
              if (selected.length === child.children.length && child.children.length !== 0) {
                SetState(child, 'isChecked');
              } else if (selected.length === child.children.length && child.children.length === 0) {
                var hulpState = child.isChecked;
                SetState(child);
                child.isChecked = hulpState;
              } else if (selected.length > 0) {
                SetState(child, 'indeterminate');
              } else {
                SetState(child);
              }
            }
            if (child.isChecked && !child.children && check === true && checkedItems.indexOf(child[$scope.searchProperty]) < 0) {
              checkedItems.push(child[$scope.searchProperty]);
            }
            //if(selected)
            hulp.push(child);
          });
          return hulp;
        }

        ctrl.onSelectItem = function (item, value, force) {

          if (item.children && item.children.length > 0) {
            handle(getAllchilds([item], [], false), true, item.isChecked);
          } else {
            handle([item], force, item.isChecked);
          }
          // Clear search 
          ctrl.searchcrit = '';
          $scope.search = ''
          // focus search
          var searchbar = angular.element('#searchbar');
          if (searchbar) searchbar.focus();
          if (item.children && item.children.length === 0) {
            return;
          }
          if (!$scope.isflat) {
            checkparent($scope.sourceCopy);
          }
        }
        ctrl.checkAll = function () {
          for (var i = 0; i < $scope.sourceCopy.length; i++) {
            $scope.sourceCopy[i].isChecked = true;
          }
          if ($scope.isflat) {
            handle($scope.sourceCopy, true, true);
          } else {
            var items = getAllchilds($scope.sourceCopy, [], false);
            handle(items, true, true);
            checkparent($scope.sourceCopy);
          }
        }

        ctrl.unCheckAll = function () {
          for (var i = 0; i < $scope.sourceCopy.length; i++) {
            $scope.sourceCopy[i].isChecked = false;
          }
          if ($scope.isflat) {
            handle($scope.sourceCopy, true, false);
          } else {
            var items = getAllchilds($scope.sourceCopy, [], false);
            handle(items, true, false);
            checkparent($scope.sourceCopy);
          }
        }
        ctrl.showOnlySelected = false;
        ctrl.showSelected = function () {
          ctrl.showOnlySelected = !ctrl.showOnlySelected;
        }
        ctrl.showAll = function () {
          ctrl.showOnlySelected = !ctrl.showOnlySelected;
        }

        ctrl.updateSearch = function (txt) {
          if (txt.length > 1) {
            ctrl.searchcrit = txt;
          } else if (txt.length === 0) {
            ctrl.searchcrit = '';
          }
        };

        function handle(items, force, forceValue) {
          var newModel = angular.copy($scope.ngModel);
          items.map(function(item) {
            item.isNew = true;
          })
          for (var i = 0; i < items.length; i++) {
            if (force) {
              items[i].isChecked = forceValue;
            }
            var index = newModel.indexOf(items[i][$scope.searchProperty]);
            if (force) {
              if (index > -1 && !forceValue) {
                newModel.splice(index, 1);
              } else if (index === -1 && forceValue) {
                newModel.push(items[i][$scope.searchProperty]);
              }
            } else if (index > -1) {
              newModel.splice(index, 1);
            } else {
              newModel.push(items[i][$scope.searchProperty]);
            }
          }
          modelCtrl.$setViewValue(newModel);
          modelCtrl.$setDirty();
        }

        //thi click elementç
        ctrl.clickHandler = function (event, doc) {
          $(document).unbind('click', ctrl.clickHandler);

          $scope.safeApply(function () {
            var isClickedElementChildOfPopup = $element
              .find(event.target)
              .length > 0;
            var isPopover = $(event.target).closest('.popover').length > 0;

            if (isClickedElementChildOfPopup) {
              if (ctrl.showSearchbar === 'true' && ctrl.editMode) {
                $timeout(function () {
                  $element.find('#searchbar').focus();
                }, 150);
              }
              if (!isPopover) {
                ctrl.editMode = !ctrl.editMode;
              }
              $timeout(function () {
                $(document).bind('click', ctrl.clickHandler);
              }, 250);
            } else {
              $(document).unbind('click', ctrl.clickHandler);
              ctrl.editMode = false;
            }
          });
        };

      }]
    };
  };
  theDirective.$inject = ['$timeout'];
  module.directive(directiveName, theDirective);
})(null);
